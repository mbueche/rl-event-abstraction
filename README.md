# Event Abstraction based on Reinforcement Learning

## About

This repository contains the implementation as described in the bachelor thesis *Exploring the Application of Reinforcement Learning to Event Abstraction in Process Mining* by M. Büche

## Setup

For each of the files, the *log_name* paramter needs to be set before running the program. The parameter must to be set to the path file of an event log to which the abstraction should be applied. (e.g. C:\event_log.xes). The event log must be in the XES format.



