import pm4py
from pm4py.objects.log.importer.xes import importer as xes_importer
from pm4py.algo.discovery.dfg import algorithm as dfg_discovery
from copy import deepcopy
import sklearn.metrics as sklearn
from pm4py.algo.discovery.inductive import algorithm as inductive_miner


# calculate silhouette coefficient
def get_sc(mapping):
    sc_log = log
    act_list = list(pm4py.get_event_attribute_values(log, "concept:name"))

    # generate distance matrix
    matrix = []
    for act in act_list:
        distance_list = []
        for act_distance_to in act_list:
            if act == act_distance_to:
                distance_list.append(0)
            else:
                dist_sum = 0
                occur_count = 0
                for trace in sc_log:
                    trace._list = sorted(trace._list, key=lambda x: x["time:timestamp"], reverse=False)
                    dist_counter = 0
                    enable_counter = False
                    for event in trace:
                        if enable_counter:
                            dist_counter += 1
                        if event["concept:name"] == act and not enable_counter:
                            enable_counter = True
                            counting_from = act
                        elif event["concept:name"] == act and enable_counter:
                            if counting_from == act:
                                dist_counter = 0
                            else:
                                dist_sum += dist_counter
                                occur_count += 1
                                dist_counter = 0
                                enable_counter = False
                        if event["concept:name"] == act_distance_to and not enable_counter:
                            enable_counter = True
                            counting_from = act_distance_to
                        elif event["concept:name"] == act_distance_to and enable_counter:
                            if counting_from == act_distance_to:
                                dist_counter = 0
                            else:
                                dist_sum += dist_counter
                                occur_count += 1
                                dist_counter = 0
                                enable_counter = False
                if occur_count != 0:
                    distance_list.append(dist_sum / occur_count)
                else:
                    distance_list.append(1)
        matrix.append(distance_list)

    labels = []
    name_list = []
    cluster_counter = 1
    for name in act_list:
        if mapping[name] not in name_list:
            name_list.append(mapping[name])
            labels.append("c" + str(cluster_counter))
            cluster_counter += 1
        else:
            index = name_list.index(mapping[name]) + 1
            labels.append("c" + str(index))

    sc = sklearn.silhouette_score(matrix, labels, metric='precomputed')
    print("Silhouette Coefficient = " + str(round(sc, 3)))


# calculate control-flow complexity
def get_cfc(net, inverse=False):
    AND = []
    XOR = []
    for place in net.places:
        if len(place.out_arcs) >= 2:
            XOR.append(len(place.out_arcs))
    for trans in net.transitions:
        if len(trans.out_arcs) >= 2:
            AND.append(1)
    if inverse:
        return 1.0 / (1.0 + sum(AND) + sum(XOR))
    else:
        return sum(AND) + sum(XOR)


def split_into_subtraces(variant):
    variants = []
    current = []
    current_labels = []
    for event in variant:
        if event["concept:name"] not in current_labels:
            current.append(event)
            current_labels.append(event["concept:name"])
        else:
            variants.append(current)
            current = [event]
            current_labels = [event["concept:name"]]
    variants.append(current)
    return variants


# apply grouping to event log
def modify_log(grouped_list):
    print("Size Reduction = " + str(round(1 - len(grouped_list) / size_orig, 2)))

    mapping = {}
    grp_counter = 1
    for act_name in grouped_list:
        name_list = act_name.split(",")
        if len(name_list) > 1:
            for name in name_list:
                mapping[name] = "Group{}".format(grp_counter)
            grp_counter += 1
        else:
            mapping[act_name] = act_name
    get_sc(mapping)

    only_complete = True
    for trace in log:
        new_trace = list()
        trace._list = sorted(trace._list, key=lambda x: x["time:timestamp"], reverse=False)
        tmp_list = split_into_subtraces(trace)
        for subtrace in tmp_list:
            for event in subtrace:
                event["concept:name"] = mapping[event["concept:name"]]
            if not only_complete:
                firsts = dict()
                for event in subtrace:
                    if event["concept:name"] not in firsts.keys():
                        new_event = deepcopy(event)
                        new_event["lifecycle:transition"] = "start"
                        firsts[event["concept:name"]] = new_event
                new_trace.extend(firsts.values())
            lasts = dict()
            reverse_iterator = reversed(subtrace)
            for event in reverse_iterator:
                if event["concept:name"] not in lasts.keys():
                    new_event = deepcopy(event)
                    new_event["lifecycle:transition"] = "complete"
                    lasts[event["concept:name"]] = new_event
            new_trace.extend(lasts.values())
        new_trace = sorted(new_trace, key=lambda x: x["time:timestamp"], reverse=False)
        trace._list = new_trace

    net, initial_marking, final_marking = inductive_miner.apply(log)
    cfc_new = get_cfc(net)
    print("Complexity Reduction = " + str(round(1 - cfc_new / cfc_orig, 2)))


class Activity:
    def __init__(self, name, id, groupedWith):
        self.name = name
        self.id = id
        self.groupedWith = groupedWith
        # partners consists of tuples: (ingoing activity name, frequency)
        self.gpartners = []
        # names of outgoing activities
        self.outgoing = []

    def __str__(self):
        return self.name + " ID:" + str(self.id) + " groupedWith:" + str(self.groupedWith)


# load log from filesystem
log_name = 'D:\\example.xes'
log = xes_importer.apply(log_name)

net_orig, initial_marking_orig, final_marking_orig = inductive_miner.apply(log)
cfc_orig = get_cfc(net_orig)
size_orig = len(list(pm4py.get_event_attribute_values(log, "concept:name")))

# generate dfg with frequencies
dfg = dfg_discovery.apply(log, variant=dfg_discovery.Variants.FREQUENCY)

# create all activity objects and put them in a list
dfg_list = list(pm4py.get_event_attribute_values(log, "concept:name"))
activites_list = []
for x in range(len(dfg_list)):
    activites_list.append(Activity(dfg_list[x], x, -1))

# add all possible grouping partners + frequency to the activity objects (ingoing and outgoing edges)
tmp = list(dfg)
for activity in activites_list:
    for x in tmp:
        if activity.name == x[1] and x[1] != x[0]:
            activity.gpartners.append((x[0], dfg[x]))
        elif activity.name == x[0] and x[1] != x[0]:
            activity.outgoing.append(x[1])

name_list = []
for act in activites_list:
    name_list.append(act.name)

# select grouping partners for each node (if relative freq > threshold)
freq_threshold = 0.6
for item in activites_list:
    sum_frequency = 0
    for partner in item.gpartners:
        sum_frequency += partner[1]
    for gpartner in item.gpartners:
        relative_freq = gpartner[1] / sum_frequency
        if relative_freq >= freq_threshold:
            for name in name_list:
                if gpartner[0] in name.split(","):
                    name_list.append(name + "," + item.name)
                    break
            name_list.remove(name)
            if item.name in name_list:
                name_list.remove(item.name)
modify_log(name_list)
