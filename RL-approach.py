import numpy as np
import pm4py
from pm4py.objects.log.importer.xes import importer as xes_importer
from pm4py.algo.discovery.dfg import algorithm as dfg_discovery
from pm4py.visualization.dfg import visualizer as dfg_visualization
from copy import deepcopy
from tkinter import *


# delete loops in traces
def split_into_subtraces(variant):
    variants = []
    current = []
    current_labels = []
    for event in variant:
        if event["concept:name"] not in current_labels:
            current.append(event)
            current_labels.append(event["concept:name"])
        else:
            variants.append(current)
            current = [event]
            current_labels = [event["concept:name"]]
    variants.append(current)
    return variants


# function called when user presses "Calculate"-button
def fetch(entries, a_list, qtable):
    global weight_size
    weight_size = float(entries[0][1].get())
    global weight_density
    weight_density = float(entries[1][1].get())
    global weight_degree
    weight_degree = float(entries[2][1].get())
    prob_matrix = deepcopy(qtable)

    result_list, final_score = agent_start(a_list, prob_matrix)
    if not final_score > 0:
        print("-------------")
        print("No grouping solution was found.")
    else:
        print("-------------")
        print("A grouping solution was found. (Solution's reward function score: " + str(final_score) + ")")

        # map low-level activities to new high-level activities
        mapping = {}
        grp_counter = 1
        for act in result_list:
            name_list = act.name.split(",")
            if len(name_list) > 1:
                act.name = "Group{}".format(grp_counter)
                grp_counter += 1
            for name in name_list:
                mapping[name] = act.name

        modified_log = xes_importer.apply(log_name)
        only_complete = True
        for trace in modified_log:
            new_trace = list()
            trace._list = sorted(trace._list, key=lambda x: x["time:timestamp"], reverse=False)
            tmp_list = split_into_subtraces(trace)
            for subtrace in tmp_list:
                for event in subtrace:
                    event["concept:name"] = mapping[event["concept:name"]]
                if not only_complete:
                    firsts = dict()
                    for event in subtrace:
                        if event["concept:name"] not in firsts.keys():
                            new_event = deepcopy(event)
                            new_event["lifecycle:transition"] = "start"
                            firsts[event["concept:name"]] = new_event
                    new_trace.extend(firsts.values())
                lasts = dict()
                reverse_iterator = reversed(subtrace)
                for event in reverse_iterator:
                    if event["concept:name"] not in lasts.keys():
                        new_event = deepcopy(event)
                        new_event["lifecycle:transition"] = "complete"
                        lasts[event["concept:name"]] = new_event
                new_trace.extend(lasts.values())
            new_trace = sorted(new_trace, key=lambda x: x["time:timestamp"], reverse=False)
            trace._list = new_trace

        # generate DFG for abstracted event log
        modified_dfg = dfg_discovery.apply(modified_log, variant=dfg_discovery.Variants.FREQUENCY)
        gviz = dfg_visualization.apply(modified_dfg, log=modified_log, variant=dfg_visualization.Variants.FREQUENCY)
        dfg_visualization.view(gviz)


def makeform(root, fields):
    entries = []
    for field in fields:
        row = Frame(root)
        lab = Label(row, width=15, text=field, anchor='w')
        ent = Entry(row)
        row.pack(side=TOP, fill=X, padx=5, pady=5)
        lab.pack(side=LEFT)
        ent.pack(side=RIGHT, expand=YES, fill=X)
        entries.append((field, ent))
    return entries


class Activity:
    def __init__(self, name, id, groupedWith):
        self.name = name
        self.id = id
        self.groupedWith = groupedWith
        # partners consists of tuples: (ingoing activity name, frequency)
        self.gpartners = []
        # names of outgoing activities
        self.outgoing = []

    def groupWith(self, id):
        self.groupedWith = id

    def __str__(self):
        return self.name + " ID:" + str(self.id) + " groupedWith:" + str(self.groupedWith)


def reward_function(list_old, list_new, orig_list):
    global weight_size
    global weight_density
    global weight_degree

    # calculate frequency factor
    score_freq_new = 0
    for item in list_new:
        tmp = item.name.split(",")
        if len(tmp) > 1:
            for i, name in reversed(list(enumerate(tmp))):
                if i == 0:
                    break
                else:
                    counter = 0
                    freq_total = 0
                    freq_grouped = 0
                    for x in orig_list:
                        if name == x.name:
                            for p in x.gpartners:
                                freq_total += p[1]
                                counter += 1
                                if p[0] == tmp[i-1]:
                                    freq_grouped = p[1]
                    if counter == 1:
                        score_freq_new += 1 / len(tmp)
                    else:
                        score_freq_new += (freq_grouped / freq_total) - (1/counter)

    # size, e.g. number of nodes
    size_new = len(list_new)

    # calculate average connector degree
    avg_degree_new = 0
    for a in list_new:
        avg_degree_new += (len(a.gpartners) + len(a.outgoing))
    avg_degree_new = avg_degree_new / size_new

    # calculate density
    density_new = 0
    for b in list_new:
        density_new += len(b.outgoing)
    density_new = density_new / (size_new * (size_new - 1))

    # size, e.g. number of nodes
    size_old = len(list_old)

    # calculate average connector degree
    avg_degree_old = 0
    for c in list_old:
        avg_degree_old += (len(c.gpartners) + len(c.outgoing))
    avg_degree_old = avg_degree_old / size_old

    # calculate density
    density_old = 0
    for d in list_old:
        density_old += len(d.outgoing)
    density_old = density_old / (size_old * (size_old - 1))

    score = weight_size * (1 - (size_new/size_old)) + weight_degree * (1 - (avg_degree_new/avg_degree_old)) + weight_density * (1 - (density_new/density_old)) + score_freq_new * 0.5
    return round(score, 3)


# takes the list of selected actions from the agent and groups activities accordingly
def generate_list_after_actions(actions, act_list):
    new_list = deepcopy(act_list)
    act_base = new_list[0]
    for x in range(len(actions)):
        if actions[x] != 0:
            act_being_grouped = new_list[x]
            for a in act_list:
                if a.name == act_being_grouped.gpartners[actions[x] - 1][0]:
                    act_base = new_list[a.id]
                    break
            # if base is already grouped go to next base until not grouped
            while act_base.groupedWith != -1:
                act_base = new_list[act_base.groupedWith]
            # don't group if the gpartner is already grouped with the activity
            if act_base.id == act_being_grouped.id:
                break
            else:
                # remove base from outgoing activities
                for obj in act_base.outgoing:
                    if obj in act_being_grouped.name.split(","):
                        act_base.outgoing.remove(obj)
                for partner in act_being_grouped.gpartners:
                    if partner[0] in act_base.name.split(","):
                        act_being_grouped.gpartners.remove(partner)
                act_being_grouped.groupedWith = act_base.id
                act_base.name = act_base.name + "," + act_being_grouped.name
                act_base.outgoing = act_base.outgoing + act_being_grouped.outgoing
                act_base.gpartners = act_base.gpartners + act_being_grouped.gpartners

    # remove all entries that were grouped
    new_list = [x for x in new_list if x.groupedWith == -1]

    # adjust IDs for new list
    id_counter = 0
    for obj in new_list:
        obj.id = id_counter
        id_counter += 1

    return new_list


# adjust probability matrix according to reward score
def adjust_policy(actions, prob_matrix, reward):
    reward_weight = 0.5
    for x in range(len(actions)):
        prob_matrix[x][actions[x]] = prob_matrix[x][actions[x]] * (1 + reward * reward_weight)


def agent_start(act_list, prob_matrix):
    # variables for handling exploration vs. exploitation
    exploration_rate = 1
    max_exploration_rate = 1
    min_exploration_rate = 0.01
    exploration_decay_rate = 0.01
    # number of rounds for the algorithm
    num_rounds = 1000
    # get size of probability matrix
    num_activities, num_pos_actions = prob_matrix.shape
    best_score = 0
    return_list = act_list

    for rounds in range(num_rounds):
        actions = []
        exploration_rate_threshold = np.random.uniform(0.0, 1.0)

        # if true, then exploit (choose actions with highest value)
        if exploration_rate_threshold > exploration_rate:
            for y in range(num_activities):
                best_action = 0.0
                best_action_index = 0
                for r in range(num_pos_actions):
                    if prob_matrix[y][r] > best_action:
                        best_action_index = r
                        best_action = prob_matrix[y][r]
                actions.append(best_action_index)
        # else explore (take random actions)
        else:
            for y in range(num_activities):
                num_possibilities = 0
                for r in range(num_pos_actions):
                    if prob_matrix[y][r] != -1:
                        num_possibilities += 1
                actions.append(np.random.randint(0, num_possibilities))
        # apply actions to activities
        changed_list = generate_list_after_actions(actions, act_list)
        # adjust policy according to the evaluation by the reward function
        if len(changed_list) > 1:
            reward_score = reward_function(return_list, changed_list, act_list)
            if reward_score > best_score:
                best_score = reward_score
                return_list = deepcopy(changed_list)
            adjust_policy(actions, prob_matrix, reward_score)

        # Exploration rate decay
        exploration_rate = min_exploration_rate + \
                           (max_exploration_rate - min_exploration_rate) * np.exp(-exploration_decay_rate * rounds)
    return return_list, best_score


# load log from filesystem
log_name = 'D:\\example.xes'
log = xes_importer.apply(log_name)

# generate dfg with frequencies
dfg = dfg_discovery.apply(log, variant=dfg_discovery.Variants.FREQUENCY)
gviz = dfg_visualization.apply(dfg, log=log, variant=dfg_visualization.Variants.FREQUENCY)
dfg_visualization.view(gviz)

# create all activity objects and put them in a list
dfg_list = list(pm4py.get_event_attribute_values(log, "concept:name"))
activites_list = []
for x in range(len(dfg_list)):
    activites_list.append(Activity(dfg_list[x], x, -1))

# add all possible grouping partners + outgoing edges + frequency to the activity objects
tmp = list(dfg)
for activity in activites_list:
    for x in tmp:
        if activity.name == x[1] and x[1] != x[0]:
            activity.gpartners.append((x[0], dfg[x]))
        elif activity.name == x[0]:
            activity.outgoing.append(x[1] and x[1] != x[0])

# create empty q-table
numcolumns = len(dfg_list)
numrows = 0

# set number of rows according to activity with most possible actions
for activity in activites_list:
    if len(activity.gpartners) > numrows:
        numrows = len(activity.gpartners)

# +1 because first field is for the option to not group at all
numrows += 1
q_table = np.zeros((numcolumns, numrows))

# fill q-table (empty spaces initialized with -1, else with 1)
tmp_count = 0
weight_frequency = 0.5
for act in activites_list:
    x = 1
    sum_frequency = 0
    sum_probabilities = 0
    for y in act.gpartners:
        sum_frequency += y[1]
    for gp in act.gpartners:
        probability = 1 + (gp[1] / sum_frequency) * weight_frequency
        q_table[tmp_count][x] = probability
        sum_probabilities += probability
        x += 1
    # take average of all probabilities for not-grouping action
    if sum_probabilities != 0:
        q_table[tmp_count][0] = sum_probabilities / len(act.gpartners)
    else:
        q_table[tmp_count][0] = 1
    tmp_count += 1
q_table[q_table == 0] = -1

# set default weights for reward function
weight_size = 4
weight_density = 1
weight_degree = 1

# GUI setup
fields = 'Size-Weight', 'Density-Weight', 'Avg-Degree-Weight'
root = Tk()
ents = makeform(root, fields)
root.bind('<Return>', (lambda event, e=ents: fetch(e, activites_list, q_table)))
b1 = Button(root, text='Calculate', command=(lambda e=ents: fetch(e, activites_list, q_table)))
b1.pack(side=LEFT, padx=5, pady=5)
b2 = Button(root, text='Quit', command=root.quit)
b2.pack(side=LEFT, padx=5, pady=5)
root.mainloop()
